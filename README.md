# Olaris Build
This is kind of a hack to build a binary. Not sure if this is useful to anyone else. You can change the repo and branch names in the `build.sh` script if necessary.

1. Build the image
   
   ```
   docker build -t olaris-build .
   ```

2. Use the image to build Olaris
   
   ```
   docker run -it --rm -v $(pwd)/dist:/dist olaris-build
   ```

Now you should have an `olaris` binary in the `./dist` directory.
