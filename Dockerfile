FROM debian:stretch

RUN apt-get -y update && \
    apt-get install -y apt-transport-https curl g++ gcc git gnupg libc6-dev make tar unzip ffmpeg && \
    curl -sL https://golang.org/dl/go1.18.linux-amd64.tar.gz | tar -C /usr/local -xz && \
    curl -sL https://deb.nodesource.com/setup_lts.x | bash - && \
    curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add - && \
    echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list && \
    apt-get -y update && apt-get install -y nodejs yarn make && \
    apt-get autoremove -y && \
    apt-get clean -y

ENV PATH="/usr/local/go/bin:${PATH}"

RUN mkdir ~/build
RUN mkdir /dist

COPY build.sh /usr/local/bin/build.sh

WORKDIR ~/build

ENTRYPOINT ["/usr/local/bin/build.sh"]
