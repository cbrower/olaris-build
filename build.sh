#!/bin/bash
git clone https://gitlab.com/olaris/olaris-server.git .
git checkout develop
make download-olaris-react
make download-ffmpeg
make build-local
cp ./build/olaris /dist/olaris
